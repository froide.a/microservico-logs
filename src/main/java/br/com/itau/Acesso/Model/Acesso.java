package br.com.itau.Acesso.Model;

public class Acesso {


    private int id;
    private int idporta;
    private int idcliente;
    
    private Boolean acessoautorizado ;

    public Boolean getAcessoautorizado() {
        return acessoautorizado;
    }

    public void setAcessoautorizado(Boolean acessoautorizado) {
        this.acessoautorizado = acessoautorizado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdporta() {
        return idporta;
    }

    public void setIdporta(int idporta) {
        this.idporta = idporta;
    }

    public int getIdcliente() {
        return idcliente;
    }

    public void setIdcliente(int idcliente) {
        this.idcliente = idcliente;
    }
}
