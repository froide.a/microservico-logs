package br.com.itau.Acesso.consumer;

import br.com.itau.Acesso.Model.Acesso;
import br.com.itau.Acesso.Mensagem.Mensagem;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class AcessoConsumer {

    @KafkaListener(topics = "spec4-alex-froide-1", groupId = "acesso")
    public void receber(@Payload Acesso acesso) {

        Mensagem mensgem = new  Mensagem();
        mensgem.guardaMensagem(acesso);

    }

}
